package com.bizup.massiveDownload;

import com.bizup.bulkDownload.BulkDownloadApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BulkDownloadApplication.class)
public class MassiveDownloadApplicationTests {

	@Test
	public void contextLoads() {
	}

}
