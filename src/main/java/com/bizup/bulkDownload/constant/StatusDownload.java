/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizup.bulkDownload.constant;

/**
 *
 * @author cgarcia
 */
public class StatusDownload {
    
    public static String IN_PROGRESS = "IN PROGRESS";
    public static String SUCESSFULL = "SUCCESSFULL";
    public static String SUCCESS_WITH_ERRORS = "SUCCESS WITH ERRORS";
    public static String FAILED = "FAILED";
    
}
