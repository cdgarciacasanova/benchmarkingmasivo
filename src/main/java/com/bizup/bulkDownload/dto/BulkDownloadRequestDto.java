/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizup.bulkDownload.dto;

import java.util.ArrayList;
import java.util.HashMap;
import javax.validation.constraints.NotNull;

/**
 *
 * @author cgarcia
 */
public class BulkDownloadRequestDto {
    
    private Long idBulkDownload;
    
    @NotNull
    private ArrayList<String> idCompanies;
    
    @NotNull
    private HashMap<String, Object> data;

    public BulkDownloadRequestDto() {
    }

    public Long getIdBulkDownload() {
        return idBulkDownload;
    }

    public void setIdBulkDownload(Long idBulkDownload) {
        this.idBulkDownload = idBulkDownload;
    }

    public ArrayList<String> getIdCompanies() {
        return idCompanies;
    }

    public void setIdCompanies(ArrayList<String> idCompanies) {
        this.idCompanies = idCompanies;
    }

    public HashMap<String, Object> getData() {
        return data;
    }

    public void setData(HashMap<String, Object> data) {
        this.data = data;
    }
    
}
