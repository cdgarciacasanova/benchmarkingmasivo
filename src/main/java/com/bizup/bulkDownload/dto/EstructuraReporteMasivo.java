/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizup.bulkDownload.dto;

/**
 *
 * @author HP
 */
public class EstructuraReporteMasivo {
    
    private String dummy;

    public String getDummy() {
        return dummy;
    }

    public void setDummy(String dummy) {
        this.dummy = dummy;
    }

    @Override
    public String toString() {
        return "EstructuraReporteMasivo{" + "dummy=" + dummy + '}';
    }
    
}
