/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizup.bulkDownload.controller;

import com.bizup.bulkDownload.constant.StatusDownload;
import com.bizup.bulkDownload.dto.BulkDownloadRequestDto;
import com.bizup.bulkDownload.dto.EstructuraReporteMasivo;
import com.bizup.bulkDownload.model.BulkDownload;
import com.bizup.bulkDownload.pdf.PDFLocalRecorder;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.concurrent.atomic.AtomicLong;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cgarcia
 */
@RestController
public class BulkDownloadController {
    
    @Autowired
    PDFLocalRecorder pdfManager;
    
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public ResponseEntity<String> greeting() {
        return ResponseEntity.ok().body("Helloooooooooooo");
    }
    
    @PostMapping("/bulkDonwload")
    //public ResponseEntity<String> makeBulkDownload(@Valid @RequestBody BulkDownloadRequestDto request) {
    public ResponseEntity<String> makeBulkDownload(@Valid @RequestBody EstructuraReporteMasivo request) {
        
        //BulkDownload bulkDownload = new BulkDownload();
        //bulkDownload.setStatus(StatusDownload.IN_PROGRESS);
        System.out.println("Resquest in API: " + request.toString());
        
        //Conversion PDF a byte[] - Prueba
        byte [] pdfbytes = null;
        try{
            System.out.println("Leyendo pdf...");
            File file = new File("C:\\pdf_prueba.pdf");
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            
            byte[] buf = new byte[1024];
            try {
                for (int readNum; (readNum = fis.read(buf)) != -1;) {
                    bos.write(buf, 0, readNum); //no doubt here is 0
                    //Writes len bytes from the specified byte array starting at offset off to this byte array output stream.
                    System.out.println("read " + readNum + " bytes,");
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
            pdfbytes = bos.toByteArray();
            pdfManager.savePDF(pdfbytes);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Excepcion...");
        }
        
        return ResponseEntity.ok().body("Helloooooooooooo");
    }
    
    
}
