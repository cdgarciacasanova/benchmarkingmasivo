/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizup.bulkDownload.model;

import com.bizup.bulkDownload.constant.StatusDownload;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Entity
@Table(name = "bulkDownload")
public class BulkDownload extends AuditModel {
    @Id
    @GeneratedValue(generator = "bulk_generator")
    @SequenceGenerator(
            name = "bulk_generator",
            sequenceName = "bulk_sequence",
            initialValue = 1000
    )
    private Long id;
    
    @Positive
    private int countFiles;
   
    @PositiveOrZero
    @Column(columnDefinition = "integer default 0")
    private int successFiles;
    
    @Column(columnDefinition = "integer default 0")
    @PositiveOrZero
    private int failFiles;
    
    @NotBlank
    @Size(min = 3, max = 10)
    private String status;

    public BulkDownload() {
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCountFiles() {
        return countFiles;
    }

    public void setCountFiles(int countFiles) {
        this.countFiles = countFiles;
    }

    public int getSuccessFiles() {
        return successFiles;
    }

    public void setSuccessFiles(int successFiles) {
        this.successFiles = successFiles;
    }

    public int getFailFiles() {
        return failFiles;
    }

    public void setFailFiles(int failFiles) {
        this.failFiles = failFiles;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
