/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizup.bulkDownload.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.lang.NonNull;

@Entity
@Table(name = "bulkDownloadCompany")
public class BulkDownloadxCompany extends AuditModel{
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private BulkDownload bulkDownload;
    
    @Id
    private String idCompany;
    
    @NotBlank
    @NonNull
    private String urlStorage;

    public BulkDownload getBulkDownload() {
        return bulkDownload;
    }

    public void setBulkDownload(BulkDownload bulkDownload) {
        this.bulkDownload = bulkDownload;
    }

    public String getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(String idCompany) {
        this.idCompany = idCompany;
    }

    public String getUrlStorage() {
        return urlStorage;
    }

    public void setUrlStorage(String urlStorage) {
        this.urlStorage = urlStorage;
    }
    
    
    
}
