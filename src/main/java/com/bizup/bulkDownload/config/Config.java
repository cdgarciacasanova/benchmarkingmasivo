/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizup.bulkDownload.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 *
 * @author HP
 */

@Configuration
public class Config {
    @Value("${appconfig.path-pdf-save}")
    private String pathPdfSave;

    public String getPathPdfSave() {
        return pathPdfSave;
    }

    public void setPathPdfSave(String pathPdfSave) {
        this.pathPdfSave = pathPdfSave;
    }

    @Override
    public String toString() {
        return "Config{" + "pathPdfSave=" + pathPdfSave + '}';
    }
    
}
