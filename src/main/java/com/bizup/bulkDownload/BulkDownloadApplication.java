package com.bizup.bulkDownload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class BulkDownloadApplication {

	public static void main(String[] args) {
		SpringApplication.run(BulkDownloadApplication.class, args);
	}

}
