/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizup.bulkDownload.pdf;

import com.bizup.bulkDownload.config.Config;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author HP
 */

@Component
public class PDFLocalRecorder implements PDFRecorder{

    @Autowired
    private Config configProperties;
    
    public PDFLocalRecorder(){
        
    }
    
    @Override
    public void openSession() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void savePDF(byte[] pdfBArray) throws FileNotFoundException, IOException {
        System.out.println("data: " + pdfBArray.toString());
        System.out.println("Grabando PDF: " + configProperties.getPathPdfSave() + "nombrepdf.pdf");
        File file = new File(configProperties.getPathPdfSave() + "nombrepdf.pdf");
        OutputStream out = new FileOutputStream(file);
        out.write(pdfBArray);
        out.close();
    }

    @Override
    public void closeSession() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void openPDF() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
