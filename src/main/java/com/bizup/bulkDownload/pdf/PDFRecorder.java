/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bizup.bulkDownload.pdf;

/**
 *
 * @author HP
 */
public interface PDFRecorder {
    
    public void openSession();
    public void savePDF(byte[] pdfBArray) throws Exception;
    public void closeSession();
    public void openPDF();
}
